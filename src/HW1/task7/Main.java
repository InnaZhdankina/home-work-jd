package HW1.task7;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        int n;
        Scanner con = new Scanner(System.in);
        n = con.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = con.nextInt();
        }
        int max = arr[0];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }
        }
        System.out.println(max);
        max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max && i != index) {
                max = arr[i];
            }
        }
        System.out.println(max);
    }
}
